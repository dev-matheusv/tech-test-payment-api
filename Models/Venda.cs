using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymentApi.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public int IdVendedor { get; set; }
        public int IdItem { get; set; }
        public string EnderecoEntrega { get; set; }
        public DateTime DataVenda { get; set; }
        public string Status { get; set; }
        public List<Item> Itens { get; set; }
    }
}