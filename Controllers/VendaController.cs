using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TechTestPaymentApi.Context;
using TechTestPaymentApi.Models;

namespace TechTestPaymentApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly OrganizadorContext _context;

        public VendaController(OrganizadorContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            try
            {
                venda.Status = "Aguardando Pagamento";
                _context.Vendas.Add(venda);
                _context.SaveChanges();

            }
            catch
            {
                return BadRequest(new
                {
                    msg = "Ocorreu um erro ao tentar criar a Venda",
                    status = HttpStatusCode.BadRequest
                });
            }

            return Created("Criado ", venda);
        }

        [HttpGet("{id}")]
        public IActionResult BuscarVendaById(int id)
        {
            var venda = _context.Vendas.Find(id);    

           if(venda is null) return BadRequest(new {
            msg = "Conteúdo inexistente, solicitação inválida",
            status = HttpStatusCode.BadRequest
        });
           // var vendaAll = _context.Vendas.
           return Ok(venda);
        }

        
    }
}