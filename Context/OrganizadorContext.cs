using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Models;

namespace TechTestPaymentApi.Context
{
    public class OrganizadorContext : DbContext
    {
        public OrganizadorContext(DbContextOptions options) : base(options)
        {
        
        }

        public DbSet<Item> Itens { get; set; } 
        public DbSet<Venda> Vendas { get; set; } = null;
        public DbSet<Vendedor> Vendedores { get; set; }
        
    }
}